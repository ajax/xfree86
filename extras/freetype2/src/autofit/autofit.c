/* $XFree86$ */

#define FT_MAKE_OPTION_SINGLE_OBJECT
#include <ft2build.h>
#include "afangles.c"
#include "afhints.c"
#include "afdummy.c"
#include "aflatin.c"
#include "afglobal.c"
#include "afloader.c"
#include "afmodule.c"
