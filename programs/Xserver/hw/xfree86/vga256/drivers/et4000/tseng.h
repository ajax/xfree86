
/* $XFree86$ */

#ifndef _TSENG_H
#define _TSENG_H

#include "vga.h"

extern int et4000_type;
extern vgaVideoChipRec ET4000;

#define TYPE_ET4000		0
#define TYPE_ET4000W32		1
#define TYPE_ET4000W32I		2
#define TYPE_ET4000W32P		3
#define TYPE_ET4000W32Pc	4
#define TYPE_ET6000		5

#endif
