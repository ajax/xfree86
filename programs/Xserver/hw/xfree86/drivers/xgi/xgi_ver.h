/* $XFree86$ */

/*****************************************************
 * xgi_ver.h
 * Generated from mkdrv shell script.
 *
 *
 *
 * Generate by 2004/12/20
 *****************************************************/



#define XGI_MAJOR_VERSION       1
#define XGI_MINOR_VERSION       2
#define XGI_PATCHLEVEL          2

#define XGIDRIVERVERSIONYEAR    5
#define XGIDRIVERVERSIONMONTH   2
#define XGIDRIVERVERSIONDAY     16
#define XGIDRIVERREVISION       1
