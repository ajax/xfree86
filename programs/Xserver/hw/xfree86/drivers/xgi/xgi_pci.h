/* $XFree86$ */

/******************************************************************
 * Define XGI new PCI device ID.
 ******************************************************************/


#define PCI_VENDOR_XGI		0x18CA
#define PCI_CHIP_XGIXG40	0x0040
#define PCI_CHIP_XGIXG20	0x0020
