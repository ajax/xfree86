/* $XFree86: xc/programs/Xserver/Xext/xf86dgaext.h,v 1.1tsi Exp $ */

#ifndef _XF86DGAEXT_H_
#define _XF86DGAEXT_H_

#include "dixstruct.h"

extern DISPATCH_PROC(ProcXF86DGADispatch);

#endif /* _XF86DGAEXT_H_ */
