/* $XFree86$ */

#ifndef _STREAM_H_
#define _STREAM_H_

/* from lbx_zlib.c */
extern unsigned long stream_out_compressed;
extern unsigned long stream_out_uncompressed;
extern unsigned long stream_out_plain;

extern unsigned long stream_in_compressed;
extern unsigned long stream_in_uncompressed;
extern unsigned long stream_in_plain;

#endif /* _STREAM_H_ */
