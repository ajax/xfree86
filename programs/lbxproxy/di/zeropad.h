/* $XFree86$ */

#ifndef _ZEROPAD_H_
#define _ZEROPAD_H_

typedef void (*ZPREQ_T) (void*);
extern ZPREQ_T ZeroPadReqVector[];

#endif /* _ZEROPAD_H_ */
