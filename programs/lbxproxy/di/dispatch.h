/* $XFree86$ */

#ifndef _DISPATCH_H_
#define _DISPATCH_H_

extern Bool lbxUseLbx;
extern Bool lbxCompressImages;
extern Bool lbxDoAtomShortCircuiting;
extern Bool lbxUseTags;
extern Bool lbxDoLbxGfx;
extern Bool lbxDoSquishing;
extern Bool resetAfterLastClient;
extern Bool terminateAfterLastClient;

#endif /* _DISPATCH_H_ */
