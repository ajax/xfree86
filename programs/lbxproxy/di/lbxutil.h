/* $XFree86$ */

#ifndef _LBXUTIL_H_
#define _LBXUTIL_H_

#ifdef LBX_STAT
extern int delta_out_total;
extern int delta_out_attempts;
extern int delta_out_hits;
extern int delta_in_total;
extern int delta_in_attempts;
extern int delta_in_hits;
#endif /* LBX_STAT */

#endif /* _LBXUTIL_H_ */
