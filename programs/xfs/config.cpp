XCOMM font server configuration file
XCOMM $XFree86$

clone-self = on
use-syslog = off
catalogue = DEFAULTFONTPATH
error-file = FSERRORS
XCOMM in decipoints
default-point-size = 120
default-resolutions = 75,75,100,100

XCOMM font cache control, specified in KB
cache-hi-mark = 2048
cache-low-mark = 1433
cache-balance = 70
