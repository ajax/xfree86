LIBRARY dmx
VERSION LIBRARY_VERSION
EXPORTS
  DMXQueryExtension
  DMXQueryVersion
  DMXSync
  DMXForceWindowCreation
  DMXGetScreenCount
  DMXGetScreenAttributes
  DMXChangeScreensAttributes
  DMXAddScreen
  DMXRemoveScreen
  DMXGetWindowAttributes
  DMXGetDesktopAttributes
  DMXChangeDesktopAttributes
  DMXGetInputCount
  DMXGetInputAttributes
  DMXAddInput
  DMXAddBackendInput
  DMXAddConsoleInput
  DMXRemoveInput
/* $XFree86$ */
