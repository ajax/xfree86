LIBRARY Xrandr
VERSION LIBRARY_VERSION
EXPORTS
 XRRFindDisplay
 XRRGetScreenInfo
 XRRQueryExtension
 XRRQueryVersion
 XRRRootToScreen
 XRRRotations
 XRRSetScreenConfig
 XRRSizes
 XRRTimes
 XRRConfigCurrentConfiguration
 XRRConfigSizes
 XRRConfigRotations
 XRRSelectInput
 XRRFreeScreenConfigInfo
 XRRUpdateConfiguration
 XRRConfigCurrentRate
 XRRConfigRates
 XRRSetScreenConfigAndRate
/* $XFree86: xc/lib/Xrandr/Xrandr-def.cpp,v 1.2 2003/03/25 04:18:12 dawes Exp $ */
